export class Plane {
  id?: any;
  owner?: string;
  model?: string;
  conso?: number;
  immat?: string;
  icao?: string;
  flights?: any;
}