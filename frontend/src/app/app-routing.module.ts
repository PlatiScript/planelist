import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPlaneComponent } from './components/add-plane/add-plane.component';
import { HomeComponent } from './components/home/home.component';
import { PlanesListComponent } from './components/planes-list/planes-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'planes', component: PlanesListComponent },
  { path: 'add', component: AddPlaneComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }