import { Component, OnInit } from '@angular/core';
import { Plane } from 'src/app/models/plane.model';
import { PlaneService } from 'src/app/services/plane.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-planes-list',
  templateUrl: './planes-list.component.html',
  styleUrls: ['./planes-list.component.css']
})
export class PlanesListComponent implements OnInit {

  planes: Plane[] = [];
  currentPlane: Plane = {};
  currentIndex = -1;
  faTrash = faTrash;

  constructor(private planeService: PlaneService) { }

  ngOnInit(): void {
    this.retrievePlanes();

  }

  retrievePlanes(): void {
    this.planeService.getAll()
      .subscribe({
        next: (data) => {
          this.planes = data;
        
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {
    this.retrievePlanes();
    this.currentPlane = {};
    this.currentIndex = -1;
  }

  setActivePlane(plane: Plane, index: number): void {
    this.currentPlane = plane;
    this.currentIndex = index;
  }
  removePlane(plane: Plane): void {
    this.planeService.delete(plane.id).subscribe({
      next: (res) => {
        console.log(res);
        this.refreshList();
      },
      error: (e) => console.error(e)
    })
  }
  removeAllPlanes(): void {
    this.planeService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }


}