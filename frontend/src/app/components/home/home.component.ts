import { Component, OnInit } from '@angular/core';
import { Plane } from 'src/app/models/plane.model';
import { PlaneService } from 'src/app/services/plane.service';
import { AirportService } from 'src/app/services/airport.service';
import { faArrowRight,faUser, faPlane } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  planes: Plane[] = [];
  loading : boolean = true;
  faArrowRight = faArrowRight
  faUser = faUser
  faPlane = faPlane

  humanco2 = 11900;

  constructor(private planeService: PlaneService,private airportService: AirportService) { }

  ngOnInit(): void {
    
    this.planeService.getAll()
      .subscribe({
        next: (data) => {
          this.planes = data;
          this.planes.map((plane) =>{
            this.planeService.getFlights(plane.icao).subscribe((response : any) => {
              plane.flights = []
              response.forEach((flight : any) =>{
                flight.date_flight = new Date(flight.firstSeen*1000).toLocaleDateString("fr-FR")
                this.airportService.get(flight.estDepartureAirport).subscribe(airportfrom => {
                  flight.airport_from = airportfrom;
                  this.airportService.get(flight.estArrivalAirport).subscribe(airportto => {
                    flight.airport_to = airportto;

                    let from_lat = flight.airport_from.coordinates.split(',')[0];
                    let from_long = flight.airport_from.coordinates.split(',')[1];

                    let to_lat = flight.airport_to.coordinates.split(',')[0];
                    let to_long = flight.airport_to.coordinates.split(',')[1];
                    flight.distance = Math.round(this.calcCrow(from_lat,from_long,to_lat,to_long))
                    if(plane.conso){
                      flight.gas = Math.round(flight.distance*plane.conso)
                    }

                    plane.flights.push(flight);
                  })
                })
                this.loading = false;
              })
            })

            return plane;
          })
        },
        error: (e) => console.error(e)
      });
  }

  calcCrow(lat1 :any, lon1:any, lat2:any, lon2:any) 
    {
      var R = 6371; // km
      var dLat = this.toRad(lat2-lat1);
      var dLon = this.toRad(lon2-lon1);
      lat1 = this.toRad(lat1);
      lat2 = this.toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
    }

    // Converts numeric degrees to radians
    toRad(Value:any) 
    {
        return Value * Math.PI / 180;
    }

}
