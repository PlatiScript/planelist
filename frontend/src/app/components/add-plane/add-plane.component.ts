import { Component } from '@angular/core';
import { Plane } from 'src/app/models/plane.model';
import { PlaneService } from 'src/app/services/plane.service';

@Component({
  selector: 'app-add-plane',
  templateUrl: './add-plane.component.html',
  styleUrls: ['./add-plane.component.css']
})
export class AddPlaneComponent {

  plane: Plane = {
    owner: '',
    model: '',
    conso: 0,
    immat: '',
    icao: ''
  };
  submitted = false;

  constructor(private planeService: PlaneService) { }

  savePlane(): void {
    const data = {
      owner: this.plane.owner,
      model: this.plane.model,
      conso: this.plane.conso,
      immat: this.plane.immat,
      icao: this.plane.icao
    };

    this.planeService.create(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
  }

  newPlane(): void {
    this.submitted = false;
    this.plane = {
      owner: '',
      model: '',
      conso: 0,
      immat: '',
      icao: ''
    };
  }

}