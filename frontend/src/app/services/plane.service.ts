import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plane } from '../models/plane.model';

const baseUrl = 'http://localhost:8080/api/planes';

@Injectable({
  providedIn: 'root'
})
export class PlaneService {

  constructor(private http: HttpClient) { }

  getFlights(icao : any) : any {
    return this.http.get<any>("https://opensky-network.org/api/flights/aircraft?icao24="+icao+"&begin=1654159868&end=1655974268&limit=25&offset=0");
  }

  getAll(): Observable<Plane[]> {
    return this.http.get<Plane[]>(baseUrl);
  }

  get(id: any): Observable<Plane> {
    return this.http.get<Plane>(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByOwner(owner: any): Observable<Plane[]> {
    return this.http.get<Plane[]>(`${baseUrl}?owner=${owner}`);
  }
}