import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plane } from '../models/plane.model';

const baseUrl = 'http://localhost:8080/api/airports';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) { }


  getAll(): Observable<any[]> {
    return this.http.get<any[]>(baseUrl);
  }

  get(code: any): Observable<any> {
    return this.http.get<any>(`${baseUrl}/${code}`);
  }

}