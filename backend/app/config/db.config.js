module.exports = {
  HOST: "database",
  USER: "planerichuser",
  PASSWORD: "planerichpass",
  DB: "planerich",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
