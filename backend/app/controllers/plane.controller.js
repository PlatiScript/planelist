const db = require("../models");
const Plane = db.planes;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  // Validate request
  if (!req.body.model) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const plane = {
    owner: req.body.owner,
    model: req.body.model,
    conso: req.body.conso,
    immat: req.body.immat,
    icao: req.body.icao
  };

  Plane.create(plane)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Plane."
      });
    });
};

exports.findAll = (req, res) => {
  const owner = req.query.owner;
  var condition = owner ? { owner: { [Op.like]: `%${owner}%` } } : null;

  Plane.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving planes."
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Plane.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Plane with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Plane with id=" + id
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Plane.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Plane was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Plane with id=${id}. Maybe Plane was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Plane with id=" + id
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Plane.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Plane was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Plane with id=${id}. Maybe Plane was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Plane with id=" + id
      });
    });
};

exports.deleteAll = (req, res) => {
  Plane.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Plane were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Plane."
      });
    });
};
