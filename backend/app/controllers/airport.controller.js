const airports = require("../../airports.json")


exports.findAll = (req, res) => {
  return res.send(airports);
};

exports.findOne = (req, res) => {
  const code = req.params.code;
  let data = airports.filter(airport => airport.ident == code)[0]
  res.send(data);
};


