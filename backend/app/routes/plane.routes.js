module.exports = app => {
  const planes = require("../controllers/plane.controller.js");

  var router = require("express").Router();

  router.post("/", planes.create);

  router.get("/", planes.findAll);

  router.get("/:id", planes.findOne);

  router.put("/:id", planes.update);
  router.delete("/:id", planes.delete);

  router.delete("/", planes.deleteAll);

  app.use('/api/planes', router);
};
