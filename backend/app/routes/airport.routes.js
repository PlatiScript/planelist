module.exports = app => {
  const airports = require("../controllers/airport.controller.js");

  var router = require("express").Router();


  router.get("/", airports.findAll);

  router.get("/:code", airports.findOne);

  app.use('/api/airports', router);
};
