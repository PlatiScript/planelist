module.exports = (sequelize, Sequelize) => {
  const Plane = sequelize.define("plane", {
    owner: {
      type: Sequelize.STRING
    },
    model: {
      type: Sequelize.STRING
    },
    conso: {
      type: Sequelize.FLOAT
    },
    immat: {
      type: Sequelize.STRING
    },
    icao: {
      type: Sequelize.STRING
    }
  });

  return Plane;
};
