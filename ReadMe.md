# PlaneRich

Plane rich est un projet ayant pour but de lister les vols de certains avions privés (jets) afin de calculer leurs emissions de CO2.

L'architecture du projet se décompose comme tel :
- Une base de donnée MySQL contenant les avions à suivres
- Une API (Express) afin de permettre de récupérer les données (avions, aéroports, ...)
- Une application web (Angular) pour pouvoir visualiser ces données.

## Installation

PlaneRich a besoin de [Docker](https://docs.docker.com) pour se déployer.

Dans un premier temps il faut récupérer le fichier docker-compose.yml à la racine du projet.
Ensuite il faut l'exécuter :
```sh
docker-compose up -d
```

Une fois le lancement des conteneurs terminés, vous pouvez accéder à localhost afin d'arriver sur la page de PlaneRich.

Par défault aucun avion n'est renseigné dans la base de données. Vous pouvez rajouter un premier avion (celui de Bernard Arnault par exemple).

Pour cela aller sur la page "Planes" et cliquez sur le bouton vert "Add" et renseigné les données ci-dessous (le champ Icao est très important).

| Formulaire | Valeur |
| ------ | ------ |
| Owner | **Bernard** |
| Model | **Bombardier Global 7500** |
| Conso | **4.6** (CO2/km) |
| Immat | **F-GVMA** |
| Icao | **395580** (important) |

Vous pouvez ensuite retourner sur la page d'accueil et voir (si il y a) les vols récent de l'avion de Bernard Arnault.
